
//
// StyleDictionaryColor.h
//
// Do not edit directly
// Generated on Wed, 22 Jan 2020 20:25:19 GMT
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, StyleDictionaryColorName) {
ColorWhite,
ColorBlack,
ColorOffBlack,
ColorNeutral5,
ColorNeutral10,
ColorNeutral50,
ColorNeutral55,
ColorNeutral70,
ColorNeutral80,
ColorBlue5,
ColorBlue10,
ColorBlue50,
ColorBlue60,
ColorBlue90,
ColorGreen50,
ColorYellow50,
ColorMagenta50,
ColorPurple50,
ColorRed5,
ColorRed50,
ColorRed55
};

@interface StyleDictionaryColor : NSObject
+ (NSArray *)values;
+ (UIColor *)color:(StyleDictionaryColorName)color;
@end
