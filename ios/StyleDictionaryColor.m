
//
// StyleDictionaryColor.m
//
// Do not edit directly
// Generated on Wed, 22 Jan 2020 20:25:19 GMT
//

#import "StyleDictionaryColor.h"


@implementation StyleDictionaryColor

+ (UIColor *)color:(StyleDictionaryColorName)colorEnum{
  return [[self values] objectAtIndex:colorEnum];
}

+ (NSArray *)values {
  static NSArray* colorArray;
  static dispatch_once_t onceToken;

  dispatch_once(&onceToken, ^{
    colorArray = @[
[UIColor colorWithRed:1.00f green:1.00f blue:1.00f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.00f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.27f green:0.23f blue:0.29f alpha:1.00f],
[UIColor colorWithRed:0.98f green:0.98f blue:0.98f alpha:1.00f],
[UIColor colorWithRed:0.90f green:0.90f blue:0.90f alpha:1.00f],
[UIColor colorWithRed:0.91f green:0.90f blue:0.91f alpha:1.00f],
[UIColor colorWithRed:0.80f green:0.80f blue:0.80f alpha:1.00f],
[UIColor colorWithRed:0.63f green:0.62f blue:0.65f alpha:1.00f],
[UIColor colorWithRed:0.36f green:0.40f blue:0.44f alpha:1.00f],
[UIColor colorWithRed:0.97f green:0.97f blue:0.99f alpha:1.00f],
[UIColor colorWithRed:0.89f green:0.91f blue:0.98f alpha:1.00f],
[UIColor colorWithRed:0.14f green:0.30f blue:0.87f alpha:1.00f],
[UIColor colorWithRed:0.12f green:0.25f blue:0.68f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.05f blue:0.26f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.76f blue:0.41f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.73f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.20f blue:0.40f alpha:1.00f],
[UIColor colorWithRed:0.54f green:0.26f blue:0.96f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.96f blue:0.96f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.16f blue:0.20f alpha:1.00f],
[UIColor colorWithRed:0.89f green:0.15f blue:0.21f alpha:1.00f]
    ];
  });

  return colorArray;
}

@end
